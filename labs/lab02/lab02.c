//  #define WOKWI             
// Uncomment if running on Wokwi RP2040 emulator.
#include <stdio.h>
#include <stdlib.h>
#include "pico/stdlib.h" //it is given but we don't need it in the wokiki sumilator
#include "pico/float.h" 
#include "pico/double.h"
//include the head file as requested

double doubleprecision(int Iterations){
  double n = 1;
  //set the initial iteration values
  double result = 1;
  //set the initial result as it will do multiplication so we cannot set 0
  while(n < Iterations){
    //while loop for keep iterations until the iterations we set
    double product = ((2*n)/((2*n)-1))*((2*n)/((2*n)+1));
    //initialize product algorithm
    result = result * product;
    //update the result
    n++;
    //update the iterations
  } 
  return 2*result;
  //return the result as the result was half of the PI
}

float singleprecision(int Iterations)
{
  float n = 1;
  //set the initial iteration values
  float result = 1;
  //set the initial result as it will do multiplication so we cannot set 0
  while(n < Iterations){
    //while loop for keep iterations until the iterations we set
    float product = ((float)(2*n)/(float)((2*n)-1))*((float)(2*n)/((float)(2*n)+1));
    //initialize product algorithm
    result = result * product;
    //update the result
    n++;
    //update the iterations
  }
  return 2*result;
  //return the result as the result was half of the PI
}
  

double error(double reference, double actualvalue){
  //insert error function to calculate error
  double error = abs(reference - actualvalue);
  //use absolute to get the positive error
  return error;
}

int main() {
  int Iterations = 100000;
  double referencePI = 3.14159265359;
#ifndef WOKWI
    // Initialise the IO as we will be using the UART
    // Only required for hardware and not needed for Wokwi
    stdio_init_all();
#endif    
    printf("Single Precision Representation: %.11f\nApproximation Error: %.11f\n", singleprecision(Iterations), error(referencePI,singleprecision(Iterations)));
    printf("Double Precision Representation: %.11f\nApproximation Error: %.11f\n", doubleprecision(Iterations), error(referencePI,doubleprecision(Iterations)));
    //just print the function to the console
    // Returning zero indicates everything went okay.
    return 0;
}
