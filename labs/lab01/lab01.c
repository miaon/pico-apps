#include "pico/stdlib.h"
//In this lab we need to use subroutine to to replace the contents 
//inside the while loop
void toggleLED(int LED_PIN, int LED_DELAY){
//define the void toggleLED() function at the top in order to 
//call it later, set parameters
  gpio_put(LED_PIN, 1);
  sleep_ms(LED_DELAY);
//when the LED pin set to 1, it blinks
  gpio_put(LED_PIN, 0);
  sleep_ms(LED_DELAY);
//when the LED pin set to 0, it stops blinking
//Toggle the LED off and then sleep for delay period
}
int main() {
    const uint LED_PIN   =  25;
    const uint LED_DELAY = 500;
//Specify the PIN number and sleep delay
    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);
//Setup the LED pin as an output.
//Do forever...
    while (true) {  
     toggleLED(LED_PIN, LED_DELAY);
//Toggle the LED on and then sleep for delay period
    }
    return 0;
}
